% This application simulates a wayward robot with random movement
% and localizes it using a Kalman filter.


robot = Robot(rand()*20-10,   ...
              rand()*20-10,   ...
              rand()*2*pi-pi, ...
              0, 0, 0);

% track poses for graph
robot_pos = [];
est_pos = [];
meas_pos = [];

kf = KalmanFilter(1.0);
              
localized = false;
distance_tolerance = 0.01;
c = 0;
max_steps = 100;
while c < max_steps && ~localized
  c = c + 1;
  
  measurement = robot.sense();
  kf.update(measurement);
  meas_pos = cat(1, meas_pos, measurement); % save for graph
  
  estimate = kf.predict();
  est_pos = cat(1, est_pos, estimate); % save for graph
 
  if randi([0 2]) == 0
    % add a random turn
    robot.move(normrnd(0, pi/4),0);
  end
  
  robot.move(0,1);
  true_position = [robot.x, robot.y];
  robot_pos = cat(1,robot_pos, true_position); % save for graph
  
  % check to see if the robot has been found.
  error = norm(estimate - true_position);
  if error <= distance_tolerance
    disp(['robot found in ', num2str(c),' steps.']);
    localized = true;
  end
  if c == max_steps
    disp('It took too many steps to localize.');
  end
end

scatter(robot_pos(:,1), robot_pos(:,2));
hold on
scatter(est_pos(:,1), est_pos(:,2), [], 'black', 'x');
%scatter(meas_pos(:,1), meas_pos(:,2), [], 'red', 's');
hold off
