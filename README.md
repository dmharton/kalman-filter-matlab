# Kalman Filter Matlab #

This is a set of Matlab scripts that use a Kalman filter to localize a lost robot. 

This was tested and verified in both Octave and Matlab.

![kf1](images/kf1.png)
![kf2](images/kf2.png)
![kf3](images/kf3.png)