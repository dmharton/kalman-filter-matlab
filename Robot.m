classdef Robot < handle
  properties
    x
    y
    heading
    turning
    distance
    turning_noise
    distance_noise
    measurement_noise
  end
  methods (Static)
    % helper function to map all angles onto [-pi, pi]
    function result = angle_trunc(a)
      while a < 0.0
        a = a + (pi * 2);
      end
      result = mod((a + pi) , (pi * 2)) - pi;
    end
  end
  methods
    function self = Robot(x, y, heading, turning_noise, distance_noise, measurement_noise)
      self.x = x;
      self.y = y;
      self.heading = heading;
      
      self.turning_noise = turning_noise;
      self.distance_noise = distance_noise;
      self.measurement_noise = measurement_noise;
    end
    function move(self, turning, distance)
      % This function turns the robot and then moves it forward.
      max_turning_angle = pi;
      
      % apply noise, this doesn't change anything if turning_noise
      % and distance_noise are zero.
      turning = normrnd(turning, self.turning_noise);
      distance = normrnd(distance, self.distance_noise);

      % truncate to fit physical limitations
      turning = max([-max_turning_angle, turning]);
      turning = min([ max_turning_angle, turning]);
      distance = max([0.0, distance]);

      % Execute motion
      self.heading = self.heading + turning;
      self.heading = self.angle_trunc(self.heading);
      self.x = self.x + distance * cos(self.heading);
      self.y = self.y + distance * sin(self.heading);
    end
    function result = sense(self)
      % Returns the robot's position plus any noise.
      x = normrnd(self.x, self.measurement_noise);
      y = normrnd(self.y, self.measurement_noise);
      result = [x y];
    end
  end
end  