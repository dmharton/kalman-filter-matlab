classdef KalmanFilter < handle
  properties
    dt % timestep length (delta time)
    x % Robot state
    P % state covariance
    F % state transition model
    H % observation model
    R % observation noise
    I % identity matrix
    Q % process noise (or how much do you trust the model?)
  end
  methods
    function self = KalmanFilter(dt)
      self.dt = dt; 
      self.x = zeros(6,1); 
      self.P = [10 0 0 0 0 0; 
                0 10 0 0 0 0;
                0 0 1 0 0 0;
                0 0 0 1 0 0;
                0 0 0 0 1 0;
                0 0 0 0 0 1];
      self.F = [1 0 dt  0 dt*dt/2       0; 
                0 1  0 dt       0 dt*dt/2;
                0 0  1  0      dt       0;
                0 0  0  1       0      dt;
                0 0  0  0       1       0;
                0 0  0  0       0       1];
      self.H = [1 0 0 0 0 0; 0 1 0 0 0 0]; 
      self.R = [1 0; 0 1]; 
      self.I = eye(6);
      self.Q = (self.F* self.F') * 0.1; 
    end
    function update(self, measurement)
      Z = measurement;
      y = Z' - (self.H * self.x);
      S = self.H * self.P * self.H' + self.R;
      K = self.P * self.H' * inv(S);
      
      self.x = self.x + K*y;
      self.P = (self.I - (K*self.H)) * self.P;
    end
    function estimate = predict(self)
      self.x = (self.F * self.x);
      self.P = self.F * self.P * self.F' + self.Q;
      estimate = [self.x(1,1), self.x(2,1)];
    end
  end
end